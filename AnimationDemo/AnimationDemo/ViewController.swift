//
//  ViewController.swift
//  AnimationDemo
//
//  Created by Elene Latsoshvili on 11/24/15.
//  Copyright © 2015 Elene Latsoshvili. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIDynamicAnimatorDelegate {
    @IBOutlet weak var gameView: BezierPathsView!
    
    
    let dropitBehavior = DropitBehavior()
    
    var attachment: UIAttachmentBehavior? {
        willSet {
            if (newValue == nil) {
                if attachment != nil {
                    animator.removeBehavior(attachment!)
                    self.gameView.setPath(nil, named: PathNames.Attachment.rawValue)
                    
                }
            }
        }
        
        didSet {
            if attachment != nil {
                animator.addBehavior(attachment!)
                attachment?.action = {
                    [unowned self] in
                    if let anchorPoint = self.attachment?.anchorPoint,
                        let center = self.attachment?.items.first?.center {
                            let path = UIBezierPath()
                            
                            path.moveToPoint(anchorPoint)
                            path.addLineToPoint(center)
                            
                            self.gameView.setPath(path, named: PathNames.Attachment.rawValue)
                    }
                }
            }
        }
    }
    
    var lastDropped: UIView?
    
    lazy var animator: UIDynamicAnimator = {
        let lazilyCreatedAnimator = UIDynamicAnimator(referenceView: self.gameView)
        lazilyCreatedAnimator.delegate = self
        return lazilyCreatedAnimator
    }()
    
    var dropsPerRow = 10
    var dropSize: CGSize {
        let size: CGFloat = gameView.bounds.width / CGFloat(dropsPerRow)
        return CGSize(width: size, height: size)
    }
    
  
    
    @IBAction func dropDragged(sender: UIPanGestureRecognizer) {
        let gesturePoint = sender.locationInView(gameView)
        
        switch sender.state {
        case .Began:
            if let lastDroppedView = lastDropped {
                attachment = UIAttachmentBehavior(item: lastDroppedView, attachedToAnchor: gesturePoint)
            }
        case .Ended: attachment = nil
        case .Changed: attachment?.anchorPoint = gesturePoint
        default: break
        }
        
        
    }
    
    @IBAction func drop(sender: UITapGestureRecognizer) {
        drop()
    }
    
    
    func drop() {
        let dropView = UIView(frame: CGRect(x: CGFloat.random(dropsPerRow) * dropSize.width, y: 0, width: dropSize.width, height: dropSize.height))
        
        dropView.backgroundColor = UIColor.random()
        dropitBehavior.addDrop(dropView)

        lastDropped = dropView
    }

    func dynamicAnimatorDidPause(animator: UIDynamicAnimator){
        removeCompletedRows()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let barrierSize = dropSize
        let barrierOrigin = CGPoint(x: gameView.bounds.midX - barrierSize.width / 2, y: gameView.bounds.midY - barrierSize.height / 2)
        
        let path = UIBezierPath(ovalInRect: CGRect(origin: barrierOrigin, size: barrierSize))
        dropitBehavior.addBarrier(path, name: PathNames.MiddleBarrier.rawValue)
        
        gameView.setPath(path, named: PathNames.MiddleBarrier.rawValue)
        
    }
    
    enum PathNames: String {
        case MiddleBarrier = "Middle Barrier"
        case Attachment = "Attachment"
    }
    
    
    
    func removeCompletedRows(){
        var dropsToRemove = [UIView]()
        var dropFrame = CGRect(x: 0, y: gameView.bounds.maxY, width: dropSize.width, height: dropSize.height)
        
        while (dropFrame.origin.y > 0) {
            var rowIsCompleted = true
            var dropsHit = [UIView]()
            for _ in 0..<dropsPerRow {
                if let hitView = gameView.hitTest(
                    CGPoint(x: dropFrame.midX, y: dropFrame.midY), withEvent: nil) {
                        if hitView.superview == gameView {
                            dropsHit.append(hitView)
                        } else {
                            rowIsCompleted = false
                        }
                }
                dropFrame.origin.x += dropSize.width
            }
            
            if (rowIsCompleted) {
                dropsToRemove += dropsHit
            }
            
            dropFrame.origin.x = 0
            dropFrame.origin.y -= dropSize.height
        }
        
        for drop in dropsToRemove {
            dropitBehavior.removeDrop(drop)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        animator.addBehavior(dropitBehavior)
    }

}

private extension CGFloat {
    static func random(max: Int) -> CGFloat {
        return CGFloat((arc4random() % UInt32(max)))
    }
}

private extension UIColor {
    static func random() -> UIColor {
        switch (arc4random() % 4) {
        case 1: return UIColor.blueColor()
        case 2: return UIColor.redColor()
        case 3: return UIColor.yellowColor()
        default: return UIColor.blackColor()
        }
    }
}





