//
//  DropitBehavior.swift
//  AnimationDemo
//
//  Created by Elene Latsoshvili on 11/24/15.
//  Copyright © 2015 Elene Latsoshvili. All rights reserved.
//

import UIKit

class DropitBehavior : UIDynamicBehavior {
    
    
    lazy var itemBehavior: UIDynamicItemBehavior = {
        let ib = UIDynamicItemBehavior()
        ib.allowsRotation = false
        return ib
    }()
    
    
    let gravity = UIGravityBehavior()
    
    lazy var collider: UICollisionBehavior = {
        var lazilyCreatedCollider = UICollisionBehavior()
//      lazilyCreatedCollider.collisionMode = .Items
        lazilyCreatedCollider.translatesReferenceBoundsIntoBoundary = true
        return lazilyCreatedCollider
    }()
    
    

    override init(){
        super.init()
        addChildBehavior(gravity)
        addChildBehavior(collider)
        addChildBehavior(itemBehavior)
        
//        action = {
//            print("behaviour executed")
//        }
    }
    
    
    func addBarrier(path: UIBezierPath, name: String){
        collider.removeBoundaryWithIdentifier(name)
        collider.addBoundaryWithIdentifier(name, forPath: path)
    }
    
    func addDrop(drop: UIView){
        dynamicAnimator?.referenceView?.addSubview(drop)
        gravity.addItem(drop)
        collider.addItem(drop)
        itemBehavior.addItem(drop)
    }
    
    func removeDrop(drop: UIView) {
        drop.removeFromSuperview()
        gravity.removeItem(drop)
        collider.removeItem(drop)
        itemBehavior.removeItem(drop)
    }

}