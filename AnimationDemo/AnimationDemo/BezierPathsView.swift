//
//  BezierPathsView.swift
//  AnimationDemo
//
//  Created by Elene Latsoshvili on 12/1/15.
//  Copyright © 2015 Elene Latsoshvili. All rights reserved.
//

import UIKit

class BezierPathsView: UIView {

    private var paths = [String : UIBezierPath]()
    
    
    func setPath(path: UIBezierPath?, named name: String){
        paths[name] = path
        setNeedsDisplay()
    }

    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        for (_, path) in paths {
            path.stroke()
        }
    }
}
