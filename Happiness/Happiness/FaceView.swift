//
//  FaceView.swift
//  Happiness
//
//  Created by Giorgi Jibladze on 9/29/15.
//  Copyright © 2015 Giorgi Jibladze. All rights reserved.
//

import UIKit

@IBDesignable class FaceView: UIView {
    
    @IBInspectable var faceColor : UIColor = UIColor.yellowColor() {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var lineWidth: CGFloat  = 3 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var scale: CGFloat = 1 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    struct Scale {
        static let eyeScaleWithFaceRadius: CGFloat = 0.3
        static let horizontalSpacing: CGFloat = 0.8
        static let verticalSpacing: CGFloat = 0.3
        static let mouthVerticalSpacing: CGFloat = 0.3
        static let mouthWidthScale: CGFloat = 0.9
        static let mouthHeightScale: CGFloat = 0.5
        static let controlPointScale: CGFloat = 0.33
    }
    
    var faceRadius: CGFloat {
        return CGFloat(min(bounds.size.width, bounds.size.height)/2) * scale;
    }
    
    var faceCenter: CGPoint {
        return convertPoint(center, fromView: superview)
    }
    
    var smileFactor: CGFloat = 0.5 {
        didSet {
            smileFactor = min(1, smileFactor)
            smileFactor = max(-1, smileFactor)
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var eyeColor: UIColor = UIColor.blackColor() {
        didSet {
            setNeedsDisplay()
        }
        
    }
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        
        let p = UIBezierPath(
            arcCenter: faceCenter,
            radius: faceRadius,
            startAngle: 0,
            endAngle: CGFloat(2*M_PI),
            clockwise: true
        )
        p.lineWidth = lineWidth
        
        eyeColor.setStroke()
        faceColor.setFill()
        
        p.stroke()
        p.fill()
        
        let eye1 = drawEyes(.Right);
        let eye2 = drawEyes(.Left);
        
        eyeColor.setFill()
        
        eye1.stroke()
        eye1.fill()
        eye2.stroke()
        eye2.fill()
        
        let mouth = drawMouth()
        
        eyeColor.setStroke()
        mouth.lineWidth = lineWidth
        mouth.stroke()
        
    }
    
    enum Eye {
        case Left
        case Right
    }
    
    func drawEyes(whichEye: Eye) -> UIBezierPath {
        let eyeRadius = faceRadius * Scale.eyeScaleWithFaceRadius
        
        var eye: UIBezierPath
        switch whichEye {
        case Eye.Left:
            eye = UIBezierPath(
                arcCenter: CGPoint(x: faceCenter.x + CGFloat(CGFloat(-1) * Scale.horizontalSpacing * faceRadius / 2),
                    y: faceCenter.y - CGFloat(Scale.verticalSpacing * faceRadius)),
                radius: eyeRadius,
                startAngle: 0,
                endAngle: CGFloat(2 * M_PI),
                clockwise: true
            )
        case Eye.Right:
            eye = UIBezierPath(
                arcCenter: CGPoint(x: faceCenter.x + CGFloat(CGFloat(1) * Scale.horizontalSpacing * faceRadius / 2),
                    y: faceCenter.y - CGFloat(Scale.verticalSpacing * faceRadius)),
                radius: eyeRadius,
                startAngle: 0,
                endAngle: CGFloat(2 * M_PI),
                clockwise: true
            )
        }
        
        return eye;
    }
    
    func drawMouth() -> UIBezierPath {
        let mouthStart: CGPoint = CGPoint(
            x: faceCenter.x - faceRadius * Scale.mouthWidthScale / 2,
            y: faceCenter.y + faceRadius * Scale.mouthVerticalSpacing
        )
        
        let mouthEnd: CGPoint = CGPoint(
            x: faceCenter.x + faceRadius * Scale.mouthWidthScale / 2,
            y: faceCenter.y + faceRadius * Scale.mouthVerticalSpacing
        )
        
        let smileHeight: CGFloat = faceRadius * Scale.mouthHeightScale * smileFactor

        let controlPoint1: CGPoint = CGPoint(
            x: mouthStart.x + faceRadius * Scale.mouthWidthScale * Scale.controlPointScale,
            y: mouthStart.y + smileHeight
        )
        
        let controlPoint2: CGPoint = CGPoint(
            x: mouthEnd.x - faceRadius * Scale.mouthWidthScale * Scale.controlPointScale,
            y: mouthEnd.y + smileHeight
        )
        
        let mouth = UIBezierPath()
        mouth.moveToPoint(mouthStart)
        mouth.addCurveToPoint(mouthEnd, controlPoint1: controlPoint1, controlPoint2: controlPoint2)
        
        return mouth
    }

}
