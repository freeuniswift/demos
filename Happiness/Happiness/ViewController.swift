//
//  ViewController.swift
//  Happiness
//
//  Created by Giorgi Jibladze on 9/29/15.
//  Copyright © 2015 Giorgi Jibladze. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    

    @IBOutlet var myFace: FaceView! {
        didSet {
            myFace.addGestureRecognizer(UIPinchGestureRecognizer(target: self, action: "pinch:"))
        }
        
    }
    
    func pinch(gesture: UIPinchGestureRecognizer) {
        if (gesture.state == .Changed) {
            myFace.scale *= gesture.scale
            gesture.scale = 1
            print(gesture.scale)
        }
    }
    
    
    @IBAction func textDidEnter(sender: UITextField) {
        if let textF = sender.text{
            if let value = NSNumberFormatter().numberFromString(textF) {
                myFace.lineWidth = CGFloat(value.doubleValue)
            }
        }
        
    }
    
    @IBOutlet var smileValue: UISlider! {
        didSet {
            myFace.smileFactor = CGFloat(smileValue.value)
        }
    }
    
    
    @IBAction func scaleChanged(sender: UISlider) {
        myFace.scale = CGFloat(sender.value);
    }
    
    
    @IBAction func smileChanged(sender: UISlider) {
        myFace.smileFactor = CGFloat(sender.value)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

