//
//  Calculator.swift
//  Calculator
//
//  Created by Giorgi Jibladze on 9/24/15.
//  Copyright © 2015 Giorgi Jibladze. All rights reserved.
//

import Foundation

class Calculator {
    
    private enum Op : CustomStringConvertible {
        case Operand(Double)
        case UnaryOperation(String, Double -> Double)
        case BinaryOperation(String, (Double, Double) -> Double)
        
        var description: String {
            get {
                switch self {
                case .Operand(let op): return "\(op)"
                case .UnaryOperation(let symb, _): return "\(symb)"
                case .BinaryOperation(let symb, _): return "\(symb)"
                }
            }
        }
    }
    
    
    
    private var operationsStack = [Op]()
    
    private var knownOperation = [String:Op]()
    
    
    init() {
        
        func learnOperation(operation: Op) {
            knownOperation["\(operation)"] = operation
        }
        
        learnOperation(Op.BinaryOperation("*", *))
        learnOperation(Op.BinaryOperation("-", {$1-$0}))
        learnOperation(Op.BinaryOperation("+", +))
        learnOperation(Op.BinaryOperation("/", {$1/$0}))
        learnOperation(Op.UnaryOperation("√", sqrt))   
    }
    
    
    
    
    func pushOperand(operand: Double) {
        operationsStack.append(Op.Operand(operand))
                print("\(operationsStack)")
    }
    
    func pushOperation(operationName: String) {
        if let operation = knownOperation[operationName] {
            operationsStack.append(operation)
        }
        
        print("\(operationsStack)")
    }
    
    
    
    private func evaluate(ops: [Op]) -> (result: Double?, remainingOps: [Op]) {
        
        if !ops.isEmpty {
            var remainingOps = ops
            let element = remainingOps.removeLast()
            
            switch element {
            case .Operand(let operand): return (operand, remainingOps)
            case .UnaryOperation(_, let operation):
                let operationEvaluation = evaluate(remainingOps)
                if let operationResult = operationEvaluation.result {
                    return (operation(operationResult), remainingOps)
                }
            
            case .BinaryOperation(_, let operation) :
                let op1Evaluation = evaluate(remainingOps)
                if let op1Result = op1Evaluation.result {
                    
                    let op2Evaluation = evaluate(op1Evaluation.remainingOps)
                    if let op2Result = op2Evaluation.result {
                        return (operation(op1Result, op2Result), op2Evaluation.remainingOps)
                    }
                }
            }
        }
        
        return (nil, ops)
    }
    
    

    func evaluate() -> Double? {
        let (result, _) = evaluate(operationsStack)
        return result
    }
    

    
    
}