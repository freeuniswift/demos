//
//  ViewController.swift
//  Calculator
//
//  Created by Giorgi Jibladze on 9/8/15.
//  Copyright (c) 2015 Giorgi Jibladze. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var number: UILabel!
    
    var userIsTypingNumber = false
    
    lazy var calculator = Calculator()
    
    @IBAction func onNumberClick(sender: UIButton) {
        
        let clickedNumber = sender.titleLabel!.text
        
        if userIsTypingNumber == true {
            number.text = number.text! + clickedNumber!
        } else {
            userIsTypingNumber = true
            number.text = clickedNumber!
        }
        
        
    }
    
    
    var displayNumber: Double {
        get {
            let num = NSNumberFormatter().numberFromString(number.text!)?.doubleValue
            return num!
        }
        
        set(newValue) {
            number.text = "\(newValue)"
        }
    }
    
    
    
    @IBAction func enterClicked() {
        calculator.pushOperand(displayNumber)
        userIsTypingNumber = false
    }
    
    
    
    
    @IBAction func performOperation(sender: UIButton) {
        
        if let operationName = sender.titleForState(.Normal) {
            calculator.pushOperation(operationName)
        }
        
        if let result = calculator.evaluate() {
            displayNumber = result
        } else {
            displayNumber = 0
        }
        
    }
    
    
    
    
    
    

}

