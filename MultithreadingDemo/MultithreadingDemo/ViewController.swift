//
//  ViewController.swift
//  MultithreadingDemo
//
//  Created by Giorgi Jibladze on 10/29/15.
//  Copyright © 2015 Giorgi Jibladze. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var imageUrl : NSURL? {
        didSet {
            fetchImage()
        }
    }
    
    
    @IBAction func load2(sender: AnyObject) {
    
        image = nil
        imageUrl = NSURL(string: "https://www.google.ge/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png")
        
    }
    
    
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    func fetchImage() {
        if let url = imageUrl {
            
            let qInt = Int(QOS_CLASS_USER_INTERACTIVE.rawValue)
            let queue = dispatch_get_global_queue(qInt, 0)
            
            loader.startAnimating()
            
            dispatch_async(queue, { () -> Void in
                let imageData = NSData(contentsOfURL: url)
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.loader.stopAnimating()
                    if imageData != nil {
                        self.image = UIImage(data: imageData!)
                    }
                })
                
            })
        }
    }
    
    
    
    
    var image : UIImage? {
        get {
            return imageObject.image
        }
        
        set {
            imageObject.image = newValue
        }
    }
    
    
    @IBOutlet weak var imageObject: UIImageView!
    
    
    
    @IBAction func loadImage(sender: AnyObject) {
        image = nil
        imageUrl = NSURL(string: "https://pbs.twimg.com/profile_images/477818086913896448/8YjL_gNP.png")
    }


}

