//
//  TweetTableViewController.swift
//  Twitter
//
//  Created by Elene Latsoshvili on 11/10/15.
//  Copyright (c) 2015 Elene Latsoshvili. All rights reserved.
//

import UIKit

class TweetTableViewController: UITableViewController, UITextFieldDelegate {
    
    
    
    var latestTwitterRequest : TwitterRequest?
    
    var nextTwitterRequest : TwitterRequest? {
        if latestTwitterRequest == nil {
            if searchText != nil {
                return TwitterRequest(search: searchText!, count: 100)
            } else {
                return nil
            }
        } else {
            return latestTwitterRequest?.requestForNewer
        }
    }
    
    @IBAction func refreshData(sender: UIRefreshControl?) {
        
        if searchText != nil {
            if let request = nextTwitterRequest {
                request.fetchTweets { (result: [Tweet]) -> Void in
                    self.latestTwitterRequest = request
                    if result.count > 0 {
                        self.tweets.insert(result, atIndex: 0)
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            self.tableView.reloadData()
                            sender?.endRefreshing()
                        })
                    }
                    sender?.endRefreshing()
                }
            }
        }
        
    }
    
    var searchText: String? {
        didSet {
            latestTwitterRequest = nil
            tweets.removeAll()
            refreshData(refreshControl)
            tableView.reloadData()
        }
    }
    
    @IBOutlet weak var searchField: UITextField! {
        didSet {
            searchField.delegate = self
        }
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if textField == searchField {
            textField.resignFirstResponder()
            searchText = textField.text
        }
        
        return true
    }
    
    
    
    
    var tweets = [[Tweet]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshData(refreshControl)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return self.tweets.count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return self.tweets[section].count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! TweetTableViewCell
        
        let tweet = self.tweets[indexPath.section][indexPath.row]
        cell.data = tweet
        
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
