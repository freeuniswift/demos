//
//  TweetTableViewCell.swift
//  Twitter
//
//  Created by Elene Latsoshvili on 11/10/15.
//  Copyright (c) 2015 Elene Latsoshvili. All rights reserved.
//

import UIKit

class TweetTableViewCell: UITableViewCell {

    
    @IBOutlet weak var tweet: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userAvatar: UIImageView!
    
    var data: Tweet? {
        didSet{
            tweet.text = data?.text
            userName.text = data?.user.name
            
            if let imageUrl = data?.user.profileImageURL {
                
                let qInt = Int(QOS_CLASS_USER_INTERACTIVE.rawValue)
                let queue = dispatch_get_global_queue(qInt, 0)
                
                dispatch_async(queue, { () -> Void in
                    let imageData = NSData(contentsOfURL: imageUrl)
                    
                    if imageData != nil {
                        let image = UIImage(data: imageData!)
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            self.userAvatar.image = image
                        })
                    }
                })
                
            }
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
