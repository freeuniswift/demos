//
//  ViewController.swift
//  AutoLayoutDemo2
//
//  Created by Elene Latsoshvili on 10/20/15.
//  Copyright (c) 2015 Elene Latsoshvili. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var loginField: UITextField!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var companyName: UILabel!
    
    private var imageViewConstraint : NSLayoutConstraint? {
        didSet {
            if let newConstraint = imageViewConstraint {
                imageView.addConstraint(newConstraint)
            }
        }
        
        willSet {
            if let existingConstraint = imageViewConstraint {
                imageView.removeConstraint(existingConstraint)
            }
        }
    }
    
    private var image: UIImage? {
        set {
            if newValue != nil{
                imageView.image = newValue
                
                let constraint : NSLayoutConstraint = NSLayoutConstraint(
                    item: imageView,
                    attribute: NSLayoutAttribute.Width,
                    relatedBy: NSLayoutRelation.Equal,
                    toItem: imageView,
                    attribute: NSLayoutAttribute.Height,
                    multiplier: (newValue?.scaleRatio)!,
                    constant: 0)
                imageViewConstraint = constraint
            }
        }
        
        get {
            return imageView.image
        }
    }
    
    @IBAction func toggleSecure() {
        secure = !secure
    }
    
    var secure = false {
        didSet {
            updateUI()
        }
    }
    
    var logedInUser: User?
    
    func updateUI(){
        passwordField.secureTextEntry = secure
        passwordLabel.text = secure ? "Secure password" : "Password"
        
        if logedInUser != nil {
            Name.text = logedInUser?.name
            companyName.text = logedInUser?.company
            self.image = logedInUser?.image
            
        }
    }
    
    
    @IBAction func login() {
        logedInUser = User.login(loginField.text ?? "", password: passwordField.text ?? "")
        updateUI()
        print(logedInUser)
    }
    
    @IBOutlet weak var passwordLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension UIImage {
    
    var scaleRatio :  CGFloat {
        get {
            if size.height != 0 {
                return size.width / size.height
            }
            return 0
        }
    }
    
}

extension User {
    var image : UIImage? {
            
        return UIImage(named: login)

    }
}
