//
//  DetailViewController.swift
//  ImageLibrary
//
//  Created by Giorgi Jibladze on 11/3/15.
//  Copyright © 2015 Giorgi Jibladze. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UIScrollViewDelegate {

    
    //@IBOutlet weak var image: UIImageView!
    
    internal var imageURL: NSURL?
    private var imageView = UIImageView()
    
    private var image: UIImage? {
        get {
            return imageView.image
        }
        set {
            imageView.image = newValue
            imageView.sizeToFit()
            scrollView?.contentSize = imageView.frame.size

        }
    }
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.delegate = self
            scrollView.minimumZoomScale = 0.03
            scrollView.maximumZoomScale = 1.0
        }
    }
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    override func viewDidLoad() {
        scrollView.addSubview(imageView)
        if let url = imageURL {
            loader.startAnimating()
            
            
            let qInt : Int = Int(QOS_CLASS_USER_INTERACTIVE.rawValue)
            let queue = dispatch_get_global_queue(qInt, 0)
            dispatch_async(queue, {
                let imageData: NSData? = NSData(contentsOfURL: url)
                
                dispatch_async(dispatch_get_main_queue(), {
                    if imageData != nil {
                        let loadedImage = UIImage(data: imageData!)
                        
                        self.image = loadedImage
                        
                        self.loader.stopAnimating()
                    }
                })
            })
            
            
        }
    }

}
