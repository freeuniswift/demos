//
//  ViewController.swift
//  ImageLibrary
//
//  Created by Giorgi Jibladze on 11/3/15.
//  Copyright © 2015 Giorgi Jibladze. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    var credentials : Dictionary<String, String> = [
        "saxeli":"password",
        "dato":"kobuladze",
        "koba":"kareli"
    ]
    
    private func checkCredentials(username: String, password: String) -> Bool{
        if let pass = credentials[username]{
            return pass == password
        }
        return false
    }

    @IBOutlet weak var username: UITextField!

    @IBOutlet weak var password: UITextField!
    @IBAction func Login() {
        if checkCredentials(username.text ?? "", password: password.text ?? "") {
            performSegueWithIdentifier("SignIn", sender: nil)
        }
    }

}

