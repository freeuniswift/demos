//
//  ImageLoaderViewController.swift
//  ImageLibrary
//
//  Created by Giorgi Jibladze on 11/3/15.
//  Copyright © 2015 Giorgi Jibladze. All rights reserved.
//

import UIKit

class ImageLoaderViewController: UIViewController {
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let dvc = segue.destinationViewController as? DetailViewController {
            dvc.imageURL = NSURL(string: "https://upload.wikimedia.org/wikipedia/commons/6/63/Tbilisi_sunset-6.jpg") ;
            
        }
    }
    
}
