//
//  HistoryViewController.swift
//  Psychologist Psychologist Psychologist Psychologist Psychologist
//
//  Created by Giorgi Jibladze on 10/15/15.
//  Copyright © 2015 Giorgi Jibladze. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController {

    var text : String = "" {
        didSet{
            textView?.text = text
        }
    }
    
    
    @IBOutlet var textView: UITextView!{
        didSet{
            textView.text = text
        }
    }
}
