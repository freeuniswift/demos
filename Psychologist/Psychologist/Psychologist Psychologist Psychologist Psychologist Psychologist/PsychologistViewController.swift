//
//  ViewController.swift
//  Psychologist Psychologist Psychologist Psychologist Psychologist
//
//  Created by Giorgi Jibladze on 10/13/15.
//  Copyright © 2015 Giorgi Jibladze. All rights reserved.
//

import UIKit

class PsychologistViewController: UIViewController {

    
    @IBAction func nothingClicked(sender: UIButton) {
        performSegueWithIdentifier("nothing", sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        var destination  : UIViewController? = nil
        if let  dest = segue.destinationViewController as? UINavigationController{
            destination = dest.visibleViewController!
        }
        if let dvc = destination as? HappinessViewController {
            if let id = segue.identifier {
                
                switch id {
                    case "totallyHappy":
                        dvc.happiness = 1
                    case "happy":
                        dvc.happiness = 0.7
                    case "sad":
                        dvc.happiness = -1
                    case "nothing":
                        dvc.happiness = 0
                    default:
                        dvc.happiness = 0
                        break
                    
                }
                
            }
        }
    }


}

