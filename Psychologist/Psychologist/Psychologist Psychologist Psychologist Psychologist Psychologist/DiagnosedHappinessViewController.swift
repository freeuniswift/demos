//
//  DiagnosedHappinessViewController.swift
//  Psychologist Psychologist Psychologist Psychologist Psychologist
//
//  Created by Giorgi Jibladze on 10/15/15.
//  Copyright © 2015 Giorgi Jibladze. All rights reserved.
//

import UIKit

class DiagnosedHappinessViewController: HappinessViewController {
    
    
    private struct historyData : CustomStringConvertible {
        var smileHistory : [Double] {
            get{
                let userDefs = NSUserDefaults.standardUserDefaults()
                if let res = userDefs.objectForKey("smileHistory"){
                    return res as! [Double]
                }
                return []
            }
            set{
                let userDefs = NSUserDefaults.standardUserDefaults()
                userDefs.setObject(newValue, forKey: "smileHistory")
            }
        }
        
        var description: String {
            get {
                var res = ""
                for s in smileHistory{
                    res += "\(s)\n"
                }
                return res
            }
        }
    }
    
    
    
   private var smileData  = historyData()
    
    
    override var happiness: Double  {
        didSet{
            smileData.smileHistory.append(happiness)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let id =  segue.identifier {
            if let dc = segue.destinationViewController as? HistoryViewController{
                switch id {
                    case "history" :
                        dc.text =  "\(smileData)"
                default: break
                }
            }
        }
    }

}
